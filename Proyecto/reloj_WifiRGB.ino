#include <FastLED.h>
#include <ESP8266WiFi.h>
#include <time.h>

#define LED_PIN 2
#define NUM_LEDS 48

const char* ssid = "Wi-Fi IPN";    //red Wifi  //INFINITUM3226_2.4
const char* password = "";    //contraseña  //1hK5xCCx0e

int ledPin = 13;
int timezone = -6 * 3600;
int dst = 0;

//colores
int c1[]={255,0,0};  //color 1
int c2[]={0,0,255};  //color 2

int rojo[]={255,0,0};
int azul[]={0,0,255};
int verde[]={0,255,0};
int amari[]={255,255,0};
int rosa[]={150,0,255};
int purp[]={51,0,102};
int cyan[]={0,255,255};
int blanc[]={255,255,255};
int negro[]={0,0,0};

CRGBArray<NUM_LEDS> leds;

WiFiServer server(80);

void setup() {
  delay(3000);
  FastLED.addLeds<WS2812, LED_PIN, GRB>(leds, NUM_LEDS);

  Serial.begin(115200);
  Serial.println("Monitor serial conectado");

  
  pinMode(ledPin,OUTPUT);
  digitalWrite(ledPin,LOW);

  Serial.println();
  Serial.print("Wifi connecting to ");
  Serial.println( ssid );

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid,password);

  Serial.println();
  
  Serial.print("Connecting");

  while( WiFi.status() != WL_CONNECTED ){
      delay(500);
      Serial.print(".");
  }
  
  digitalWrite( ledPin , HIGH);
  Serial.println();

  server.begin();
  Serial.println("Wifi Connected Success!");
  Serial.print("NodeMCU IP Address : ");
  Serial.println(WiFi.localIP() );

  configTime(timezone, dst, "pool.ntp.org","time.nist.gov");
  Serial.println("\nWaiting for Internet time");

 /* while(!time(nullptr)){
     Serial.print("*");
     delay(1000);
  }
  Serial.println("\nTime response....OK");
*/
}

//para reloj con 48 LEDs en vez de 60 (multiplicar los minutos por 'am' y las horas por 4 en vez de 5)
int am= 1*48/60;

void loop() { 
  WiFiClient client = server.available(); 
  //leer hora
  time_t now = time(nullptr);
  struct tm* p_tm = localtime(&now);

  Serial.print(p_tm->tm_mday);
  Serial.print("/");
  Serial.print(p_tm->tm_mon + 1);
  Serial.print("/");
  Serial.print(p_tm->tm_year + 1900);
  
  Serial.print(" ");
  
  Serial.print(p_tm->tm_hour);
  Serial.print(":");
  Serial.print(p_tm->tm_min);
  Serial.print(":");
  Serial.println(p_tm->tm_sec);

  //LEDS
  for(int i=0; i<=NUM_LEDS -1; i++){
    if(i<=(p_tm->tm_hour)*4 && i>=(p_tm->tm_min)*am)
    {
      leds[i]=CRGB(c1[0],c1[1],c1[2]);
      if(i<=(p_tm->tm_min)*am){
        leds[i]=CRGB(c2[0],c2[1],c2[2]);
        FastLED.show();
        
      }
    }
    //else if(/*i<=(p_tm->tm_hour)*4 && */i<=((p_tm->tm_min)*am))
    //{
      //leds[i]=CRGB(c2[0],c2[1],c2[2]);
    //}
    else{
      leds[i]=CRGB(c2[0],c2[1],c2[2]);
    }
  }
    
  if((p_tm->tm_hour)*4==0){
    leds[0]=CRGB(c1[0],c1[1],c1[2]);
  }
  else{
  //leds[(p_tm->tm_hour)*5]=CRGB(c1[0],c1[1],c1[2]);
  }
//  leds[p_tm->tm_min]=CRGB(c2[0],c2[1],c2[2]);
  FastLED.show();
  
  leerRequests(client);

  //se resetean LEDS del mismo ciclo
  leds[(p_tm->tm_hour)*4]=CRGB(0,0,0);
  leds[(p_tm->tm_min)*am]=CRGB(0,0,0);
  
  delay(1000);
  //rutina hora
  /*
  for(int i=0; i<= NUM_LEDS-1; i=i+5){
    leds[i]=CRGB(c1[0],c1[1],c1[2]);
    FastLED.show();    
    for(int j =0; j<= NUM_LEDS-1; j++){
      leerRequests(client);
      leds[j]=CRGB(c2[0],c2[1],c2[2]);
      FastLED.show();
      delay(100);
      leds[j]=CRGB(0,0,0);
      leds[i]=CRGB(c1[0],c1[1],c1[2]);
      FastLED.show();    
      }
    leds[i]=CRGB(0,0,0);
  } 
  */ 

}


void leerRequests(WiFiClient client){
  //leer requests
  String request = client.readStringUntil('\r');
  if (request.indexOf("\favicon")==-1){
  //  Serial.println("New client");
  //  Serial.println("request");

    client.println("HTTP/1.1 200 oc?");
    client.println("Content-type: text/html");
    client.println("");
    client.println("<!DOCTYPE HTML>");
    client.println("<html>");
  }
  client.flush();

  leerColor2(request);
}

//funcion no casteada, para cambiar color con puerto serial
/*
void leerColor(){
  //cambiar colores
  char tema=Serial.read();
  if(tema=='1')
  {
    Serial.println("Cambiando a tema 1 (rojo / azul)");
    cambiarColor(1);
  }
  if(tema=='2')
  {
    Serial.println("Cambiando a tema 2 (purpura / verde)");
    cambiarColor(2);
  }
  if(tema=='3')
  {
    Serial.println("Cambiando a tema 3 (azul / purpura)");
    cambiarColor(3);
  }
  if(tema=='4')
  {
    Serial.println("Cambiando a tema 4 (verde / amarillo)");
    cambiarColor(4);
  }
}
*/

void leerColor2(String request){
  //cambiar colores
  Serial.println("New client request");
  if(request.indexOf("/tema1") != -1)
  {
    Serial.println("Cambiando a tema 1 (rojo / azul)");
    cambiarColor(1);
  }
  if(request.indexOf("/tema2") != -1)
  {
    Serial.println("Cambiando a tema 2 (purpura / verde)");
    cambiarColor(2);
  }
  if(request.indexOf("/tema3") != -1)
  {
    Serial.println("Cambiando a tema 3 (cyan / rosa)");
    cambiarColor(3);
  }
  if(request.indexOf("/tema4") != -1)
  {
    Serial.println("Cambiando a tema 4 (verde / amarillo)");
    cambiarColor(4);
  }
  if(request.indexOf("/tema5") != -1)
  {
    Serial.println("Cambiando a tema 5 (blanco / negro)");
    cambiarColor(5);
  }
}


void cambiarColor(int c){
  //temas de colores
  //1 rojo / azul
  if(c==1){
    for(int j=0; j<=2; j++){
      c1[j]=rojo[j];
      c2[j]=azul[j];
    }
  }
  //2 purpura / verde
  if(c==2){
    for(int j=0; j<=2; j++){
      c1[j]=purp[j];
      c2[j]=verde[j];
    }
  }
  //3 azul / purpura
  if(c==3){
    for(int j=0; j<=2; j++){
      c1[j]=cyan[j];
      c2[j]=rosa[j];
    }
  }
  //2 verde / amarillo
  if(c==4){
    for(int j=0; j<=2; j++){
      c1[j]=verde[j];
      c2[j]=amari[j];
    }
  }
  if(c==5){
    for(int j=0; j<=2; j++){
      c1[j]=blanc[j];
      c2[j]=negro[j];
    }
  }
  Serial.println("Tema cambiado con éxito!");
}
