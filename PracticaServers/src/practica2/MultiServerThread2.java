package practica2;

import java.net.*;
import java.io.*;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;

import static practica2.ClientFullDuplex.conexionCliente;

public class MultiServerThread2 extends Thread {
   private Socket socket = null;
   private String[] ServicesDirectory1= {"inv","concat","may","imeca"};
   private String[] ServicesDirectory2= {"min","num2text","dec2bin","RecUltPost","postear"};

   public MultiServerThread2(Socket socket) {
      super("MultiServerThread2");
      this.socket = socket;
      ServerMultiClient.NoClients++;
   }

   public void run() {
      try {
         PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
         BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         String lineIn, lineOut;
		
	while((lineIn = entrada.readLine()) != null){
            System.out.println("Received: "+lineIn);
            escritor.flush();                   
            if(lineIn.equals("FIN")){
                ServerMultiClient.NoClients--;
                break;
            }
            String[] partes = lineIn.split("#");
            
            char[] cadena = partes[3].toCharArray();
            char[] cadena2 = partes[3].toCharArray();
            String cadenacon="",respuesta,texto;
             HttpResponse re;
            int aux=0,i=0,rect=0,poop;
                    
            switch(partes[1])
            {
              /*
                case "inv":
                    for(aux=cadena.length-1;aux>=0;aux--)
                    {
                        cadena2[i]=cadena[aux];   
                        i++;
                    }
                    escritor.println("#R:inv#"+String.valueOf(cadena2));
                break;
                case "concat":
                    for(aux=3;aux<=Integer.parseInt(partes[2])+2;aux++)
                    {
                        cadenacon=cadenacon+partes[aux];
                    }
                    escritor.println("#R:concat#"+String.valueOf(cadenacon));
                break;
                case "may":
                    for(aux=0;aux<cadena.length;aux++)
                    {
                        cadena2[aux] = (char)(cadena[aux]-32);
                    }
                    escritor.println("#R:may#"+String.valueOf(cadena2));
                break;
             */
                case "min":
                    for(aux=0;aux<cadena.length;aux++)
                    {
                        cadena2[aux] = (char)(cadena[aux]+32);
                    }
                    escritor.println("#R:min#"+String.valueOf(cadena2));
                break;
                case "num2text":
                    escritor.println("#R:num2text#"+numeros.cantidadConLetra(partes[3]));
                break;  
                case "dec2bin":
                    aux = Integer.parseInt(partes[3]);
                    escritor.println("#R:dec2bin#"+Integer.toBinaryString(aux));
                break;
                case "RecUltPost":
                    escritor.println("#R:RecUltPos#"+practica2.scraping.scrap("face"));
                break;
                case "postear":
                    texto = scraping.scrap("imeca");
                    re =Request.Post("https://graph.facebook.com/v3.1/2068994736461758/feed/").bodyForm(Form.form().add("message",texto).add("access_token","EAAeqVZBTOE0ABAKjMtucuUJvPoQcZC7uYWHjUGtqzW34eGk50kFZCwdjljyTXblF4IcflWZB3TIsZCC9iaPyAhvP5dngvZCcL9m23qkZBbqMVLEkKz8wS0zQae30qyEdVVuyKeCVthZAkHAH9gA8x0GSG6nF89g0Q51q3uMuZA3jHgBP6mZARFMF1lZAGlFUE7BuSA5FhwxBP0PBwZDZD").build()).execute().returnResponse();
                    escritor.println("#R:postear#1#"+re);
                break;
                
                
                default:
                    //escritor.println("No se detecto el codigo en Server 1");
                    poop = ServicesDirectory1.length-1;
                    System.out.println("No se detecto el codigo en Server 2");
                    for(int u = 0;u <= poop;u++)
                    {
                        if(partes[1].equals(ServicesDirectory1[u])) {
                            rect = 1;
                            //System.out.println(rect);
                        }    
                    }
                    if(rect==1)
                    {
                        System.out.println("Detectado en Server 1; haciendo conexion");
                        String[] solicitud = {"12345",lineIn};
                        respuesta = conexionCliente(solicitud);
                        
                        rect=0; 
                        escritor.println(respuesta);
                    }
                    else
                    {
                        escritor.println("El comando no se encontro en ningun Server Disponible");
                    }
                    break;
            } 
            i=0;
            aux=0;
            escritor.flush();
        } 
         try{		
            entrada.close();
            escritor.close();
            socket.close();
         }catch(Exception e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	   } 
      }catch (IOException e) {
         e.printStackTrace();
      }
   }
} 
