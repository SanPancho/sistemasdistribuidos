package practica2;

import java.net.*;
import java.io.*;

import static practica2.ClientFullDuplex.conexionCliente;

public class MultiServerThread extends Thread {
   private Socket socket = null;
   private String[] ServicesDirectory1= {"inv","concat","may","imeca"};
   private String[] ServicesDirectory2= {"min","num2text","dec2bin","RecUltPost","postear"};

   public MultiServerThread(Socket socket) {
      super("MultiServerThread");
      this.socket = socket;
      ServerMultiClient.NoClients++;
   }

   public void run() {
      try {
         PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
         BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         String lineIn, lineOut;
         
	
        while((lineIn = entrada.readLine()) != null){
            System.out.println("Received: "+lineIn);
            escritor.flush();     
            String[] partes = lineIn.split("#");
            if(lineIn.equals("FIN")){
                ServerMultiClient.NoClients--;
                break;
            }
            if(lineIn.equals("#")){
                escritor.println("#R:NoConexiones#"+ServerMultiClient.NoClients);
            }
            char[] cadena = partes[3].toCharArray();
            char[] cadena2 = partes[3].toCharArray();
            String cadenacon="",respuesta;
            int aux=0,i=0,rect=0,poop;
            if(clases.isDefinedLocal(1,partes[1])==1)
            {
                switch(partes[1])
                {
                    /*
                    case "inv":
                        for(aux=cadena.length-1;aux>=0;aux--)
                        {
                            cadena2[i]=cadena[aux];   
                            i++;
                        }
                        escritor.println("#R:inv#"+String.valueOf(cadena2));
                        break;
                    case "concat":
                        for(aux=3;aux<=Integer.parseInt(partes[2])+2;aux++)
                        {
                            cadenacon=cadenacon+partes[aux];
                        }
                        escritor.println("#R:concat#"+String.valueOf(cadenacon));
                        break;
                    */
                    case "may":
                        for(aux=0;aux<cadena.length;aux++)
                        {
                            cadena2[aux] = (char)(cadena[aux]-32);
                        }
                        escritor.println("#R:may#"+String.valueOf(cadena2));
                        break;
                    case "min":
                     for(aux=0;aux<cadena.length;aux++)
                        {
                        cadena2[aux] = (char)(cadena[aux]+32);
                        }
                        escritor.println("#R:min#"+String.valueOf(cadena2));
                        break;
                     /*
                     case "imeca":
                        escritor.println("#R:RecUltPos#"+scraping.scrap("imeca"));
                        break;
                       
                        
                        case "num2text":
                        escritor.println("#R:num2text#"+numeros.cantidadConLetra(partes[3]));
                        break;
                        case "dec2bin":
                        aux = Integer.parseInt(partes[3]);
                        escritor.println("#R:dec2bin#"+Integer.toBinaryString(aux));
                        break;
                        */
                    default:
                        //escritor.println("No se detecto el codigo en Server 1");
                        poop = ServicesDirectory2.length-1;
                        System.out.println("No se detecto el codigo en Server 1");
                        for(int u = 0;u <= poop;u++)
                        {
                            if(partes[1].equals(ServicesDirectory2[u])) {
                                rect = 1;
                                //System.out.println(rect);
                            }    
                        }
                        if(rect==1)
                        {
                            System.out.println("Detectado en Server 2; haciendo conexion");
                            String[] solicitud = {"12346",lineIn};
                            respuesta = conexionCliente(solicitud);

                            rect=0; 
                            escritor.println(respuesta);
                        }
                        else
                        {
                            escritor.println("El comando no se encontro en ningun Server Disponible");
                        }
                        break;
                }i=0;
                aux=0;
                escritor.flush();
            }
            else
            {
                if(clases.isDefinedGlobal(partes[1])==0)
                {
                  escritor.println("El comando no se encontro en ningun Server Disponible");
                }
                else
                {
                    
                }
            }
            
            
            
            
        } 
         try{		
            entrada.close();
            escritor.close();
            socket.close();
         }catch(Exception e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	   } 
      }catch (IOException e) {
         e.printStackTrace();
      }
   }
} 
