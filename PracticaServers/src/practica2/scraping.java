package practica2;

import java.io.IOException;
import java.io.*;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class scraping 
{
    public static String url = "";
    public static int maxPages = 2;
    public static String texto = "";
    
    public static String scrap(String s)  
    {
        for (int i=1; i<maxPages; i++){
            if(s.equals("face"))
            {
                url = "https://www.facebook.com/ActitudPolitecnica/";
                String urlPage = String.format(url, i);
                System.out.println("Comprobando entradas de: "+urlPage);

                if (getStatusConnectionCode(urlPage) == 200) {

                    Document document = getHtmlDocument(urlPage);
                    String pag = document.toString();
                    int inicio=0;
                    inicio= pag.indexOf("<p>");
                    int fin=0;
                    fin = pag.indexOf("</p>");

                    texto = pag.substring(inicio + 3, fin);	
                }else{
                    System.out.println("El Status Code no es OK es: "+getStatusConnectionCode(urlPage));
                    break;
                }
            }
            else
            {
                url = "http://aqicn.org/city/mexico/mexico/gustavo-a.-madero/es/";
                String urlPage = String.format(url, i);
                System.out.println("Comprobando entradas de: "+urlPage);
                if (getStatusConnectionCode(urlPage) == 200) {
                    Document document = getHtmlDocument(urlPage);
                    Elements entradas = document.select("div.aqiwidget-table-x");
                    for (Element elem : entradas) {
                        String indice = elem.getElementsByClass("aqiwgt-table-aqiinfo").text();		
                       
                        texto =indice;	
                    }

                }else{
                    System.out.println("El Status Code no es OK es: "+getStatusConnectionCode(urlPage));
                    break;
                }
            }
            
        }
        return texto;
    }
    
  
    public static int getStatusConnectionCode(String url) {
		
        Response response = null;
		
        try {
            response = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(100000).ignoreHttpErrors(true).execute();
        } catch (IOException ex) {
            System.out.println("Excepción al obtener el Status Code: " + ex.getMessage());
        }
        return response.statusCode();
    }
	
    public static Document getHtmlDocument(String url) {

        Document doc = null;

        try {
            doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(100000).get();
        } catch (IOException ex) {
            System.out.println("Excepción al obtener el HTML de la página" + ex.getMessage());
        }

        return doc;

    }
}
