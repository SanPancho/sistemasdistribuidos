import java.net.*;
import java.io.*;
import java.util.Date;
import Servicios.Servicios;

public class MultiServerThread extends Thread {
   private Socket socket = null;
   private Servicios service = null;

   public MultiServerThread(Socket socket) {
      super("MultiServerThread");
      this.socket = socket;
      ServerMultiClient.NoClients++;
      this.service = new Servicios();
   }

   public void run() {
       
       service.registrar_servicio("#crupt#");
       service.registrar_servicio("#M#");
       service.registrar_servicio("#m#");
       service.registrar_servicio("#I#");
       service.registrar_servicio("#B#");
       service.registrar_servicio("#clima#");
       
              
        try {
            PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String lineIn, lineOut;


            while((lineIn = entrada.readLine()) != null){
                lineOut = service.buscar_servicio(lineIn);
                escritor.flush();
                if(!lineOut.isEmpty()){
                    escritor.println(lineOut);
                    escritor.flush();
                    System.out.println("Received: "+lineIn);
                }else if(lineIn.equals("FIN")){
                    ServerMultiClient.NoClients--;
                    break;
                }
            } 
            try{		
                entrada.close();
                escritor.close();
                socket.close();
            }catch(Exception e){ 
                System.out.println ("Error : " + e.toString()); 
                socket.close();
                System.exit (0); 
            } 
        }catch (IOException e) {
           e.printStackTrace();
        }
    }
} 
