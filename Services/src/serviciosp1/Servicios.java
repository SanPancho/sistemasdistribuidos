/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servicios;


import java.util.ArrayList;
import java.util.List;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;

/**
 *
 * @author Israel Josafat Perez
 */
public class Servicios {
        public static int getStatusConnectionCode(String url) {
		
        Response response = null;
		
        try {
            response = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(100000).ignoreHttpErrors(true).execute();
        } catch (IOException ex) {
            System.out.println("Excepción al obtener el Status Code: " + ex.getMessage());
        }
        return response.statusCode();
    }
	
	
    /**
     * Con este método devuelvo un objeto de la clase Document con el contenido del
     * HTML de la web que me permitirá parsearlo con los métodos de la librelia JSoup
     * @param url
     * @return Documento con el HTML
     */
    public static Document getHtmlDocument(String url) {

        Document doc = null;

        try {
            doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(100000).get();
        } catch (IOException ex) {
            System.out.println("Excepción al obtener el HTML de la página" + ex.getMessage());
        }

        return doc;

    }
    
    public List<String> servicio;
    public static final String url = "http://tiempoytemperatura.es/mexico/ciudad-de-mexico.html#por-horas";

    public Servicios() {
        this.servicio = new ArrayList<String>();
    }
    
    public void registrar_servicio(String Service){
        this.servicio.add(Service);
    }
    
    public String buscar_servicio(String Service){
        char hashtag = '#';
        String NO, comando, cadena,inv;
        comando = "";
        cadena = "";
        inv = "";
        NO = "No se encontro servicio";
        int x;
        
        for(int i = 1; i <= Service.length();i++){
            if(Service.charAt(i)== hashtag){
               comando = Service.substring(0, i+1);
               cadena = Service.substring(i+1, Service.length());
               break;
            }
        }
        if(Service.charAt(0)== hashtag && this.servicio.contains(comando)){
            switch(comando){
                case "#crupt#":
                    return cadena.getBytes().toString();
                case "#M#":
                    return cadena.toUpperCase();
                case "#m#":
                    return cadena.toLowerCase();
                case "#I#":
                    for(int i = cadena.length()-1;i >= 0;i--){
                        inv = inv + cadena.charAt(i);
                    }
                    return inv;
                case "#B#":
                    x= Integer.parseInt(cadena);
                    cadena = Integer.toBinaryString(x);
                    return cadena;
                    
                case "#clima#":
                    String urlPage = String.format(url);
                    System.out.println("Comprobando entradas de: "+urlPage);

                    // Compruebo si me da un 200 al hacer la petición
                    if (getStatusConnectionCode(urlPage) == 200) {

                        // Obtengo el HTML de la web en un objeto Document2
                        Document document = getHtmlDocument(urlPage);

                        // Busco todas las historias de meneame que estan dentro de: 
                        Elements entradas = document.select("div.today");
                        // Paseo cada una de las entradas
                        for (Element elem : entradas) 
                        {
                            String temp = elem.getElementsByClass("temp").text();
                            System.out.println("Temperatura: "+temp);			
                            String hum = elem.getElementsByClass("value").text();
                            System.out.println("Humedad: "+hum);			
                        }

                    }else{
                        System.out.println("El Status Code no es OK es: "+getStatusConnectionCode(urlPage));
                    }

                    
            }
            
        }else{
            NO = NO+" "+comando+" "+cadena+" "+Service;
            return NO;
        }           
        return null;
    } 
}
